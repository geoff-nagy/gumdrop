#include "gumdrop.h"

#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdint.h>
using namespace std;

GumdropNode::GumdropNode()
{
	// nothing to do
}

GumdropNode::~GumdropNode()
{
	vector<GumdropNode*>::iterator i;
	vector<GumdropVariable*>::iterator j;

	for(i = nodes.begin(); i != nodes.end(); ++ i)
	{
		delete *i;
	}

	for(j = variables.begin(); j != variables.end(); ++ j)
	{
		delete *j;
	}
}

GumdropNode *GumdropNode::fromFile(const string &filename)
{
	GumdropNode *result = NULL;
	uint8_t *data;
	uint32_t len;
	ifstream file;

	// attempt to open the file at the end
	file.open(filename.c_str(), ios::binary | ios::ate);
	if(file.is_open())
	{
		// find file size
		len = file.tellg();
		file.seekg(0, ios::beg);

		// allocate space and attempt to read the entire file at once
		data = new uint8_t[len];
		if(file.read((char*)data, len))
		{
			result = fromMemory(data, len);
			result -> name = filename;
		}

		// clean up
		file.close();
		delete[] data;
	}

	return result;
}

GumdropNode *GumdropNode::fromMemory(uint8_t *data, uint32_t len)
{
	const uint8_t STATE_EXPECT_NAME = 1;								// node or var name expected
	const uint8_t STATE_EXPECT_OPEN_BRACE_OR_EQUALS = 2;				// read name; need open brace or equal sign
	const uint8_t STATE_EXPECT_CLOSE_BRACE_OR_NAME = 3;					// read a var or node; need a var or the end of the node
	const uint8_t STATE_EXPECT_SINGLE_OR_ARRAY_OPEN_BRACE = 4;			// read an equals; need a single value or an array opening brace
	const uint8_t STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE = 5;			// read an array value; need a comma for next value or an array close brace
	const uint8_t STATE_EXPECT_ARRAY_VALUE = 6;							// read a comma; the next token MUST be an array value
	const uint8_t STATE_EXPECT_ARRAY_VALUE_OR_CLOSE_BRACE = 7;			// read a comma or an opening brace; need next array value

	GumdropNode *root = new GumdropNode();
	GumdropNode *curr = root;
	GumdropNode *newNode;
	string lastName;
	DataType lastDataType;
	GumdropSingleVariable *varSingle = NULL;
	GumdropArrayVariable *varArray = NULL;

	uint8_t state = STATE_EXPECT_NAME;
	bool done = false;

	string token;
	uint32_t index = 0;
	uint32_t lineNum = 1;
	int32_t braceDepth = 0;

	while(!done)
	{
		token = getNextToken(data, len, &index, &lineNum);
		if(token.size() > 0)
		{
			if(state == STATE_EXPECT_NAME)
			{
				if(isValidName(token))
				{
					state = STATE_EXPECT_OPEN_BRACE_OR_EQUALS;
					lastName = token;
				}
				else if(token.compare("}") == 0)
				{
					// we might have an empty node, which is fine; back up the tree and become the parent
					if(braceDepth >= 0)
					{
						curr = curr -> parent;
					}
					else
					{
						cerr << "[line " << lineNum << "]: too many closing braces were encountered at this point" << endl;
						return NULL;
					}

					// decrement brace depth
					-- braceDepth;
				}
				else
				{
					cerr << "[line " << lineNum << "]: expected a valid node name here, not \"" << token << "\"" << endl;
					return NULL;
				}
			}
			else if(state == STATE_EXPECT_OPEN_BRACE_OR_EQUALS)
			{
				if(token.compare("=") == 0)
				{
					state = STATE_EXPECT_SINGLE_OR_ARRAY_OPEN_BRACE;
				}
				else if(token.compare("{") == 0)
				{
					// open a new node
					state = STATE_EXPECT_NAME;
					++ braceDepth;

					// create the node
					newNode = new GumdropNode();
					newNode -> name = lastName;
					newNode -> parent = curr;

					// set as child of the current node
					curr -> nodes.push_back(newNode);

					// set as most recent node
					curr = newNode;
				}
				else
				{
					cerr << "[line " << lineNum << "]: expected \"=\" or \"{\", not \"" << token << "\"" << endl;
					return NULL;
				}
			}
			else if(state == STATE_EXPECT_CLOSE_BRACE_OR_NAME)
			{
				if(token.compare("}") == 0)
				{
					// back up the tree and become the parent
					if(braceDepth >= 0)
					{
						curr = curr -> parent;
					}
					else
					{
						cerr << "[line " << lineNum << "]: too many closing braces were encountered at this point" << endl;
						return NULL;
					}

					// close the current node
					state = STATE_EXPECT_CLOSE_BRACE_OR_NAME;
					-- braceDepth;
				}
				else
				{
					if(isValidName(token))
					{
						state = STATE_EXPECT_OPEN_BRACE_OR_EQUALS;
						lastName = token;
					}
					else
					{
						cerr << "[line " << lineNum << "]: expected a valid variable name here, not \"" << token << "\"" << endl;
						return NULL;
					}
				}
			}
			else if(state == STATE_EXPECT_SINGLE_OR_ARRAY_OPEN_BRACE)
			{
				if(token.compare("{") == 0)
				{
					state = STATE_EXPECT_ARRAY_VALUE_OR_CLOSE_BRACE;
					varArray = new GumdropArrayVariable();
					varArray -> name = lastName;
					curr -> variables.push_back(varArray);
				}
				else if(getDataType(token) != DATA_TYPE_INVALID)
				{
					lastDataType = getDataType(token);
					if(lastDataType == DATA_TYPE_BOOL)
					{
						state = STATE_EXPECT_NAME;
						varSingle = new GumdropSingleVariable();
						varSingle -> name = lastName;
						varSingle -> dataType = DATA_TYPE_BOOL;
						varSingle -> boolValue = convertToBool(token);
						curr -> variables.push_back(varSingle);
					}
					else if(lastDataType == DATA_TYPE_DOUBLE)
					{
						state = STATE_EXPECT_NAME;
						varSingle = new GumdropSingleVariable();
						varSingle -> name = lastName;
						varSingle -> dataType = DATA_TYPE_DOUBLE;
						varSingle -> doubleValue = convertToDouble(token);
						curr -> variables.push_back(varSingle);
					}
					else if(lastDataType == DATA_TYPE_INT)
					{
						state = STATE_EXPECT_NAME;
						varSingle = new GumdropSingleVariable();
						varSingle -> name = lastName;
						varSingle -> dataType = DATA_TYPE_INT;
						varSingle -> intValue = convertToInt(token);
						curr -> variables.push_back(varSingle);
					}
					else if(lastDataType == DATA_TYPE_STRING)
					{
						state = STATE_EXPECT_NAME;
						varSingle = new GumdropSingleVariable();
						varSingle -> name = lastName;
						varSingle -> dataType = DATA_TYPE_STRING;
						varSingle -> strValue = convertToString(token);
						curr -> variables.push_back(varSingle);
					}
					else
					{
						cerr << "[line " << lineNum << "]: expected a valid constant value here, not \"" << token << "\"" << endl;
						return NULL;
					}
				}
				else
				{
					cerr << "[line " << lineNum << "]: expected the beginning of an array or a constant value, not \"" << token << "\"" << endl;
					return NULL;
				}
			}
			else if(state == STATE_EXPECT_ARRAY_VALUE_OR_CLOSE_BRACE)
			{
				lastDataType = getDataType(token);
				if(token.compare("}") == 0)
				{
					state = STATE_EXPECT_NAME;
				}
				else if(lastDataType == DATA_TYPE_BOOL)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_BOOL;
					varSingle -> boolValue = convertToBool(token);
					varArray -> elements.push_back(varSingle);
				}
				else if(lastDataType == DATA_TYPE_DOUBLE)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_DOUBLE;
					varSingle -> doubleValue = convertToDouble(token);
					varArray -> elements.push_back(varSingle);
				}
				else if(lastDataType == DATA_TYPE_INT)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_INT;
					varSingle -> intValue = convertToInt(token);
					varArray -> elements.push_back(varSingle);
				}
				else if(lastDataType == DATA_TYPE_STRING)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_STRING;
					varSingle -> strValue = convertToString(token);
					varArray -> elements.push_back(varSingle);
				}
				else
				{
					cerr << "[line " << lineNum << "]: expected an array value, not \"" << token << "\"" << endl;
					return NULL;
				}
			}
			else if(state == STATE_EXPECT_ARRAY_VALUE)
			{
				lastDataType = getDataType(token);
				if(lastDataType == DATA_TYPE_BOOL)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_BOOL;
					varSingle -> boolValue = convertToBool(token);
					varArray -> elements.push_back(varSingle);
				}
				else if(lastDataType == DATA_TYPE_DOUBLE)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_DOUBLE;
					varSingle -> doubleValue = convertToDouble(token);
					varArray -> elements.push_back(varSingle);
				}
				else if(lastDataType == DATA_TYPE_INT)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_INT;
					varSingle -> intValue = convertToInt(token);
					varArray -> elements.push_back(varSingle);
				}
				else if(lastDataType == DATA_TYPE_STRING)
				{
					state = STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE;
					varSingle = new GumdropSingleVariable();
					varSingle -> name = lastName;
					varSingle -> dataType = DATA_TYPE_STRING;
					varSingle -> strValue = convertToString(token);
					varArray -> elements.push_back(varSingle);
				}
				else
				{
					cerr << "[line " << lineNum << "]: expected an array value, not \"" << token << "\"" << endl;
					return NULL;
				}
			}
			else if(state == STATE_EXPECT_COMMA_OR_ARRAY_CLOSE_BRACE)
			{
				if(token.compare(",") == 0)
				{
					state = STATE_EXPECT_ARRAY_VALUE;
				}
				else if(token.compare("}") == 0)
				{
					state = STATE_EXPECT_NAME;
				}
				else
				{
					cerr << "[line " << lineNum << "]: expected \",\" or \"}\", not \"" << token << "\"" << endl;
					return NULL;
				}
			}
		}
		else
		{
			done = true;
		}
	}

	// make sure braces align
	if(braceDepth > 0)
	{
		cerr << "[line " << lineNum << "]: end of data reached, but a brace is still open" << endl;
		return NULL;
	}
	else if(braceDepth < 0)
	{
		cerr << "[line " << lineNum << "]: end of data reached, but there are too many closing braces" << endl;
		return NULL;
	}

	return root;
}

string GumdropNode::toString()
{
	stringstream ss;
	vector<GumdropVariable*>::iterator i;
	vector<GumdropNode*>::iterator k;

	// print variables first
	for(i = variables.begin(); i != variables.end(); ++ i)
	{
		ss << (*i) -> toString(0);
	}

	// now print child nodes
	for(k = nodes.begin(); k != nodes.end(); ++ k)
	{
		ss << (*k) -> toStringChild(0);
	}

	return ss.str();
}

void GumdropNode::toFile(const string &filename)
{
	ofstream file;
	string data = toString();

	// attempt to open the file at the end
	file.open(filename.c_str(), ios::binary);
	if(file.is_open())
	{
		file.write(data.c_str(), data.size());
		file.close();
	}
}

bool GumdropNode::getBool(const string &path, bool defaultValue, bool *found, bool enableError)
{
	bool result = defaultValue;
	GumdropSingleVariable *var = getVar(path, DATA_TYPE_BOOL);

	if(var)
	{
		result = var -> boolValue;
	}
	else if(enableError)
	{
		cerr << "\"" << path << "\": could not find bool here" << endl;
		exit(1);
	}

	if(found) *found = var;

	return result;
}

double GumdropNode::getDouble(const string &path, double defaultValue, bool *found, bool enableError)
{
	double result = defaultValue;
	GumdropSingleVariable *var = getVar(path, DATA_TYPE_DOUBLE);

	if(var)
	{
		result = var -> doubleValue;
	}
	else if(enableError)
	{
		cerr << "\"" << path << "\": could not find double here" << endl;
		exit(1);
	}

	if(found) *found = var;

	return result;
}

int32_t GumdropNode::getInt(const string &path, int32_t defaultValue, bool *found, bool enableError)
{
	int32_t result = defaultValue;
	GumdropSingleVariable *var = getVar(path, DATA_TYPE_INT);

	if(var)
	{
		result = var -> intValue;
	}
	else if(enableError)
	{
		cerr << "\"" << path << "\": could not find int here" << endl;
		exit(1);
	}

	if(found) *found = var;

	return result;
}

string GumdropNode::getString(const string &path, const string &defaultValue, bool *found, bool enableError)
{
	string result = defaultValue;
	GumdropSingleVariable *var = getVar(path, DATA_TYPE_STRING);

	if(var)
	{
		result = var -> strValue;
	}
	else if(enableError)
	{
		cerr << "\"" << path << "\": could not find string here" << endl;
		exit(1);
	}

	if(found) *found = var;

	return result;
}

string GumdropNode::toStringChild(uint32_t indent)
{
	stringstream ss;
	vector<GumdropVariable*>::iterator i;
	vector<GumdropNode*>::iterator k;
	uint32_t j;

	// name and opening brace
	for(j = 0; j < indent; ++ j) ss << "\t";
	ss << name << endl;
	for(j = 0; j < indent; ++ j) ss << "\t";
	ss << "{" << endl;

	// print variables first
	for(i = variables.begin(); i != variables.end(); ++ i)
	{
		ss << (*i) -> toString(indent + 1);
	}

	// now print child nodes
	for(k = nodes.begin(); k != nodes.end(); ++ k)
	{
		ss << (*k) -> toStringChild(indent + 1);
	}

	// closing brace
	for(j = 0; j < indent; ++ j) ss << "\t";
	ss << "}" << endl;

	return ss.str();
}

GumdropNode::GumdropSingleVariable *GumdropNode::getVar(const string &path, DataType dataType)
{
	const uint8_t STATE_READ_NAME = 0;
	const uint8_t STATE_READ_INDEX = 1;
	const uint8_t STATE_READ_DOT = 2;
	const uint8_t STATE_DONE = 3;

	GumdropNode *curr;
	vector<pair<string, int32_t> > indexers;
	vector<pair<string, int32_t> >::iterator i;

	GumdropSingleVariable *result = NULL;
	string tempPath = path + '.';
	string nameStr = "";
	string indexStr = "";
	char ch;
	int32_t index;
	uint8_t state = STATE_READ_NAME;
	uint32_t ptr = 0;

	// process the path char by char
	while(state != STATE_DONE && ptr < tempPath.size())
	{
		ch = tempPath[ptr++];
		if(state == STATE_READ_NAME)
		{
			if(ch != '.' && ch != '[')
			{
				// encountered a non-delimiting char
				if(ch != ' ' && ch != '\t') nameStr += ch;
			}
			else if(ch == ']')
			{
				cerr << "\"" << path << "\": closing index bracket with no prior opening bracket" << endl;
				exit(1);
			}
			else
			{
				// was this a valid name?
				if(nameStr.size() > 0)
				{
					// make sure the name follows node and variable name syntax
					if(isValidName(nameStr))
					{
						// add the entry
						if(ch == '.')
						{
							state = STATE_READ_NAME;
							indexers.push_back(make_pair(nameStr, 0));
							nameStr = "";
						}
						else
						{
							// opening index bracket detected
							state = STATE_READ_INDEX;
						}
						index = 0;
					}
					else
					{
						cerr << "\"" << path << "\": the name \"" << nameStr << "\" is not syntactically allowed" << endl;
						exit(1);
					}
				}
				else
				{
					cerr << "\"" << path << "\": node or variable names cannot be blank" << endl;
					exit(1);
				}
			}
		}
		else if(state == STATE_READ_INDEX)
		{
			if(ch != ']' && ch != '[')
			{
				// encountered a non-delimiting char
				if(ch != ' ' && ch != '\t') indexStr += ch;
			}
			else if(ch == '[')
			{
				cerr << "\"" << path << "\": opening bracket in already-open index specifier" << endl;
				exit(1);
			}
			else
			{
				// valid indices include either (a) blank indices (defaults to index 0) or positive integer indices
				if(indexStr.size() > 0)
				{
					// make sure we have an integer type
					if(getDataType(indexStr) == DATA_TYPE_INT)
					{
						// make sure the index is zero or greater
						if((index = convertToInt(indexStr)) >= 0)
						{
							// add the entry
							state = STATE_READ_DOT;
							indexers.push_back(make_pair(nameStr, index));
							index = 0;
							indexStr = "";
							nameStr = "";
						}
						else
						{
							cerr << "\"" << path << "\": the index " << index << " of \"" << nameStr << "\" must be greater than zero" << endl;
							exit(1);
						}
					}
					else
					{
						cerr << "\"" << path << "\": the index \"" << indexStr << "\" of \"" << nameStr << "\" must be an integer" << endl;
						exit(1);
					}
				}
				else
				{
					cerr << "\"" << path << "\": the index of \"" << nameStr << "\" cannot be blank" << endl;
					exit(1);
				}
			}
		}
		else if(state == STATE_READ_DOT)
		{
			if(ch == '.')
			{
				state = STATE_READ_NAME;
			}
			else
			{
				cerr << "\"" << path << "\": expected \".\", not \"" << ch << "\"" << endl;
				exit(1);
			}
		}
	}

	// go through all nodes; these should be all but the final entry in the given path
	curr = this;
	i = indexers.begin();
	while(curr != NULL && i < indexers.end() - 1)
	{
		curr = curr -> getNodeByName(i -> first, i -> second);
		++ i;
	}

	// if we found the right node, look for the variable
	if(curr)
	{
		result = curr -> getVarByName(i -> first, i -> second, dataType);
	}

	// debugging: advance through our pairs of names and indices
	/*for(i = indexers.begin(); i != indexers.end(); ++ i)
	{
		cout << i -> first << ": " << i -> second << endl;
	}*/

	return result;
}

// this is a state machine that tokenizes the given string and returns the next token starting from a given index
string GumdropNode::getNextToken(uint8_t *data, uint32_t len, uint32_t *index, uint32_t *lineNum)
{
	const uint8_t STATE_READ_WHITESPACE = 0;
	const uint8_t STATE_READ_LINE_REMAINDER = 1;
	const uint8_t STATE_READ_ANYTHING = 2;
	const uint8_t STATE_READ_INT = 3;
	const uint8_t STATE_READ_FLOAT = 4;
	const uint8_t STATE_READ_VAR_CHAR = 5;
	const uint8_t STATE_READ_STRING = 6;
	const uint8_t STATE_DONE = 7;

	uint8_t state;
	string token = "";
	bool escaped = false;
	char ch;

	// read from the starting index we were given
	state = STATE_READ_WHITESPACE;
	while(state != STATE_DONE && *index < len)
	{
		ch = data[(*index)++];

		// skip over any leading whitespace
		if(state == STATE_READ_WHITESPACE)
		{
			if(ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n')
			{
				if(ch == '\n') (*lineNum)++;
				state = STATE_READ_WHITESPACE;
			}
			else
			{
				state = STATE_READ_ANYTHING;
			}
		}

		// now read an actual non-whitespace token
		if(state == STATE_READ_LINE_REMAINDER)
		{
			if(ch == '\n')
			{
				(*lineNum)++;
				state = STATE_READ_WHITESPACE;
			}
		}
		else if(state == STATE_READ_ANYTHING)
		{
			if(ch == '#')
			{
				state = STATE_READ_LINE_REMAINDER;
			}
			else if(ch == '{' || ch == '}' || ch == '=' || ch == ',')
			{
				token += ch;
				state = STATE_DONE;
			}
			else if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z'))
			{
				token += ch;
				state = STATE_READ_VAR_CHAR;
			}
			else if(ch == '-' || (ch >= '0' && ch <= '9'))
			{
				token += ch;
				state = STATE_READ_INT;
			}
			else if(ch == '.')
			{
				token += ch;
				state = STATE_READ_FLOAT;
			}
			else if(ch == '"')
			{
				token += ch;
				state = STATE_READ_STRING;
			}
			else if(ch == ' ' || ch == '\t' || ch == '\r' || ch == '\n')
			{
				if(ch == '\n') (*lineNum)++;
				state = STATE_DONE;
			}
			else
			{
				cerr << "[line " << *lineNum << "]: \"" << ch << "\" was not expected here" << endl;
				exit(1);
			}
		}
		else if(state == STATE_READ_INT)
		{
			if(ch >= '0' && ch <= '9')
			{
				token += ch;
				state = STATE_READ_INT;
			}
			else if(ch == '.')
			{
				token += ch;
				state = STATE_READ_FLOAT;
			}
			else if(ch == '}' || ch == ',' || ch == '\r' || ch == '\n' || ch == '\t' || ch == ' ')
			{
				(*index)--;
				state = STATE_DONE;
			}
			else if(ch == '#')
			{
				(*index)--;
				state = STATE_DONE;
				//state = STATE_READ_LINE_REMAINDER;
			}
			else
			{
				cerr << "[line " << *lineNum << "]: \"" << ch << "\" was not expected here" << endl;
				exit(1);
			}
		}
		else if(state == STATE_READ_FLOAT)
		{
			if(ch >= '0' && ch <= '9')
			{
				token += ch;
				state = STATE_READ_FLOAT;
			}
			else if(ch == '}' || ch == ',' || ch == '\r' || ch == '\n' || ch == '\t' || ch == ' ')
			{
				(*index)--;
				state = STATE_DONE;
			}
			else if(ch == '#')
			{
				(*index)--;
				state = STATE_READ_LINE_REMAINDER;
			}
			else
			{
				cerr << "[line " << *lineNum << "]: \"" << ch << "\" was not expected here" << endl;
				exit(1);
			}
		}
		else if(state == STATE_READ_STRING)
		{
			if(escaped)
			{
				if     (ch == '"') token += ch;
				else if(ch == 'r') token += '\r';
				else if(ch == 'n') token += '\n';
				else if(ch == 't') token += '\t';
				else if(ch == 'b') token += '\b';
				else if(ch == '\\') token += '\\';
				else
				{
					cerr << "[line " << *lineNum << "]: escape character \"\\" << ch << "\" is not recognized" << endl;
					exit(1);
				}
				escaped = false;
			}
			else
			{
				if(ch == '"')
				{
					token += ch;
					state = STATE_DONE;
				}
				else if(ch == '\\')
				{
					escaped = true;
				}
				else if(ch == '\r' || ch == '\n')
				{
					cerr << "[line " << *lineNum << "]: this line contains an unterminated string" << endl;
					exit(1);
				}
				else
				{
					token += ch;
					state = STATE_READ_STRING;
				}
			}
		}
		else if(state == STATE_READ_VAR_CHAR)
		{
			if((ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z') || (ch >= '0' && ch <= '9') || ch == '_' || (ch >= '0' && ch <= '9'))
			{
				token += ch;
				state = STATE_READ_VAR_CHAR;
			}
			else
			{
				(*index)--;
				state = STATE_DONE;
			}
		}
	}

	return token;
}

GumdropNode *GumdropNode::getNodeByName(const std::string &name, uint32_t index)
{
	GumdropNode *result = NULL;
	GumdropNode *curr;
	vector<GumdropNode*>::iterator i = nodes.begin();
	int32_t count = -1;

	while(!result && i != nodes.end() && count < (int)index)
	{
		curr = *i++;
		if(curr -> name.compare(name) == 0)
		{
			++ count;
			if(count == (int)index)
			{
				result = curr;
			}
		}
	}

	return result;
}

GumdropNode::GumdropSingleVariable *GumdropNode::getVarByName(const std::string &name, uint32_t index, DataType dataType)
{
	GumdropSingleVariable *result = NULL;
	GumdropVariable *curr;
	GumdropArrayVariable *arr;
	vector<GumdropVariable*>::iterator i;
	int32_t count;

	// first, look for a single variable of this type
	i = variables.begin();
	count = -1;
	while(!result && i != variables.end() && count < (int)index)
	{
		curr = *i++;
		if(curr -> name.compare(name) == 0 && curr -> dataType == dataType)
		{
			++ count;
			if(count == (int)index)
			{
				result = dynamic_cast<GumdropSingleVariable*>(curr);
				if(!result)
				{
					cerr << "\"" << name << "\": internal error; single cast failed" << endl;
					exit(1);
				}
			}
		}
	}

	// if there was no single var matching this, then look for an array instead
	if(!result)
	{
		i = variables.begin();
		while(!result && i != variables.end())
		{
			curr = *i++;
			if(curr -> name.compare(name) == 0 && curr -> dataType == DATA_TYPE_ARRAY)
			{
				// convert to array type to examine
				arr = dynamic_cast<GumdropArrayVariable*>(curr);
				if(arr)
				{
					// does the array size and specified element type match?
					if(index < arr -> elements.size() && arr -> elements[index] -> dataType == dataType)
					{
						result = arr -> elements[index];
					}
				}
				else
				{
					cerr << "\"" << name << "\": internal error; array cast failed" << endl;
					exit(1);
				}
			}
		}
	}

	return result;
}

GumdropNode::DataType GumdropNode::getDataType(const string &value)
{
	char *p = 0;
	string strData;
	DataType type = DATA_TYPE_INVALID;

	if(value.size() > 0)
	{
		strtol(value.c_str(), &p, 10);
		if(*p)
		{
			strtod(value.c_str(), &p);
			if(*p)
			{
				if(value[0] == '\"' && value[value.size() - 1] == '\"')
				{
					type = DATA_TYPE_STRING;
				}
				else if(value.compare("false") == 0 || value.compare("true") == 0)
				{
					type = DATA_TYPE_BOOL;
				}
			}
			else
			{
				type = DATA_TYPE_DOUBLE;
			}
		}
		else
		{
			type = DATA_TYPE_INT;
		}
	}

	return type;
}

bool GumdropNode::convertToBool(const std::string &value)
{
	return value.compare("true") == 0;
}

double GumdropNode::convertToDouble(const std::string &value)
{
	return atof(value.c_str());
}

int32_t GumdropNode::convertToInt(const std::string &value)
{
	return atoi(value.c_str());
}

string GumdropNode::convertToString(const std::string &value)
{
	return value.substr(1, value.size() - 2);
}

bool GumdropNode::isValidName(const string &name)
{
	bool result = name.size() > 0;
	uint32_t i = 0;

	while(result && i < name.size())
	{
		result = (name[i] >= 'a' && name[i] <= 'z') || (name[i] >= 'A' && name[i] <= 'Z') || name[i] == '_' || (i > 0 && name[i] >='0' && name[i] <='9');
		++ i;
	}

	return result;
}

// - - - GumdropVariable implementation - - - //

GumdropNode::GumdropVariable::GumdropVariable()
	: dataType(DATA_TYPE_INVALID) { }

GumdropNode::GumdropVariable::~GumdropVariable() { }

// - - - GumdropSingleVariable implementation - - - //

GumdropNode::GumdropSingleVariable::GumdropSingleVariable() { }

GumdropNode::GumdropSingleVariable::~GumdropSingleVariable() { }

string GumdropNode::GumdropSingleVariable::toString(uint32_t indent)
{
	uint32_t i;
	stringstream ss;

	for(i = 0; i < indent; ++ i) ss << "\t";
	ss << name << " = ";
	if     (dataType == DATA_TYPE_BOOL) ss << (boolValue ? "true" : "false");
	else if(dataType == DATA_TYPE_DOUBLE) ss << doubleValue;
	else if(dataType == DATA_TYPE_INT) ss << intValue;
	else if(dataType == DATA_TYPE_STRING) ss << "\"" << strValue << "\"";
	ss << endl;

	return ss.str();
}

// - - - GumdropArrayVariable implementation - - - //

GumdropNode::GumdropArrayVariable::GumdropArrayVariable()
{
	dataType = DATA_TYPE_ARRAY;
}

GumdropNode::GumdropArrayVariable::~GumdropArrayVariable()
{
	vector<GumdropSingleVariable*>::iterator i;

	for(i = elements.begin(); i != elements.end(); ++ i)
	{
		delete *i;
	}
}

string GumdropNode::GumdropArrayVariable::toString(uint32_t indent)
{
	stringstream ss;
	vector<GumdropSingleVariable*>::iterator i;
	uint32_t j;

	for(j = 0; j < indent; ++ j) ss << "\t";
	ss << name << " = {";
	for(i = elements.begin(); i != elements.end(); ++ i)
	{
		if     ((*i) -> dataType == DATA_TYPE_BOOL) ss << ((*i) -> boolValue ? "true" : "false");
		else if((*i) -> dataType == DATA_TYPE_DOUBLE) ss << (*i) -> doubleValue;
		else if((*i) -> dataType == DATA_TYPE_INT) ss << (*i) -> intValue;
		else if((*i) -> dataType == DATA_TYPE_STRING) ss << "\"" << (*i) -> strValue << "\"";

		if(i < elements.end() - 1) ss << ", ";
	}
	ss << "}" << endl;

	return ss.str();
}
