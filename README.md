# Gumdrop Data Format

## Introduction
Over the years I've used JSON, XML, and a few other data formats (including some simple ones of my own) but have struggled to find one that is both (a) general-purpose enough without being syntactically overwhelming or ugly, and (b) easy to access programmatically in a one-line-ish fashion. Gumdrop is my answer to both (a) and (b).

It's roughly C-esque and structured similarly to JSON. Here's an example:

```python
# examples/example1.gum

# this is a Gumdrop "node"
example
{
	# another Gumdrop node nested inside of it; you can nest these as deep as you want
	more_data
	{
		# there are four elementary data types: bools, int32s, doubles, and strings
		allows_bools = true              # bools are either true or false (case sensitive)
		example_int = 5                  # ints must be in the range -2147483648 to 2147483647
		example_double = 2.0             # doubles *must* have a decimal, or are interpreted as ints
		example_string = "hello world"   # strings *must* use double-quotes

		# type mixing is allowed inside of arrays
		example_array = {1, 2.2, "test", true}

		# multiply-nested arrays are not supported, although you can nest nodes arbitrarily

		# multiply-defined variables and/or arrays are allowed
		example_int = 6
		example_int = {7, 8, 9}
	}

	# multiply-defined nodes are also allowed
	more_data
	{
		example_int = 10
	}
}
```

## Accessing the Data

Data can be accessed in two ways. The first way is to use a compile-time string to specify the path to a variable:

```c++
// will exit with return code 1 and a descriptive message to standard error if the file couldn't be parsed
GumdropNode *file = GumdropNode::fromFile("examples/example1.gum");

// try to find a double called example_double, but return -5.0 if we can't find it
cout << file -> getDouble("example.more_data.example_double", -5.0) << endl;                 // prints 2.0

// try to find an integer called example_int, but return -1 if we can't find it
cout << file -> getInt("example.more_data.example_int", -1) << endl;                         // prints 5

// try to find the 2nd integer called example_int, but return -1 if we can't find it
cout << file -> getInt("example.more_data.example_int[1]", -1) << endl;                      // prints 6
```

The second way is to build the path string programmatically during run-time:

```c++
cout << file -> getInt(GUMDROP("example.more_data.example_int[" << 1 << "]", 0)) << endl;    // prints 6
```

(The `GUMDROP()` macro is just a Gumdrop-provided wrapper for a C++ `stringstream` that is easier than defining an extra `stringstream` variable.)

Syntactically-invalid path strings will cause the program to exit with error code 1, and a descriptive message will be output to standard error.

An **optional** third parameter can be used to indicate if the data is not found (this is `NULL` by default):

```c++
bool found;

// access the second node named "more_data"
cout << file -> getInt("example.more_data[1].example_int", -1, &found) << endl;              // prints 10

if(!found)
{
	cout << "could not find value!" << endl;
}
```

An **optional** fourth parameter, when set to `true`, will exit with an error message if the data was not found (this is `false` by default):

```c++
cout << file -> getString("example.more_data.example_string", "", NULL, true) << endl;       // "hello world"
```

## Naming Variables and Nodes

Variable and node names can contain lower- or upper-case letters and underscores. Numbers are only allowed after the first character. Other characters are not allowed. There are no reserved words.

## Dealing with Ambiguity

Gumdrop allows multiply-defined nodes. Multiply-defined nodes are accessed using square brackets:

```c++
cout << file -> getInt("example.more_data.example_int", -1) << endl;                        // prints 5
cout << file -> getInt("example.more_data[0].example_int", -1) << endl;                     // prints 5

cout << file -> getInt("example.more_data[1].example_int", -1) << endl;                     // prints 10
cout << file -> getInt("example[0].more_data[1].example_int", -1) << endl;                  // prints 10
```

Gumdrop allows multiply-defined variables and arrays. These are also accessed using square brackets.

It can become confusing when the same name is used to refer to instances of multiply-defined variables where an array (or even arrays) of the same name is/are also used:

```python
# Gumdrop file:
values
{
	data_int = 10
	data_int = 11
	data_int = 12
	data_int = {"hello", "world", 13, 14}        # the 3rd value, 13, is inaccessible
	data_int = false
}
```
```c++
// C++ file:
// [...]
cout << file -> getInt("values.data_int[0]", 0) << endl;            // prints 10
cout << file -> getInt("values.data_int[1]", 0) << endl;            // prints 11
cout << file -> getInt("values.data_int[2]", 0) << endl;            // prints 12, never 13
cout << file -> getInt("values.data_int[3]", 0) << endl;            // prints 14

cout << file -> getString("values.data_int[0]", "") << endl;        // prints hello
cout << file -> getString("values.data_int[1]", "") << endl;        // prints world

cout << file -> getBool("values.data_int[0]", true) << endl;        // prints 0 (for false)
```

Note that the 3rd value in the `data_int` array above, `13`, is inaccessible. This is because single (non-array) variables of the correct data type will always have precedent over array elements of the same data type.

If a matching single variable of the requested data type is not found, array variables are then scanned for a matching data type at the given index. This is why `"values.data_int[3]"` above evaluates to 14.

If an array, variable, or node index is not specified, it is assumed to be 0.

## Escape Sequences in Strings

The following escape sequences are supported in strings.

| Escape Sequence | Meaning |
| --------------- | ------- |
| \\"             | Double quote |
| \r              | Carriage return |
| \n              | New line |
| \t              | Tab |
| \b              | Back space |
| \\\\            | Back slash |

No other escape sequences are considered valid, and will throw an error.

## Bug Reports and Other Concerns

If you encounter a bug, or an error in the documentation, please email the developer at [geoff.nagy@gmail.com](mailto:geoff.nagy@gmail.com).

## End of Readme
