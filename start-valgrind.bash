#!/bin/bash

valgrind --tool=memcheck --leak-check=full --show-leak-kinds=all --undef-value-errors=yes --expensive-definedness-checks=yes --track-origins=yes --gen-suppressions=yes --suppressions=valgrind-suppressions.txt ./main $1 $2 $3 $4 $5 $6 $7 $8 $9 ${10} ${11}

