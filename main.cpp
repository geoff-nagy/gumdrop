#include "gumdrop.h"

#include <iostream>
#include <string>
#include <assert.h>
using namespace std;

// - - - prototypes - - - //

void runUnitTests();

// - - - main function - - - //

int main(int args, char *argv[])
{
	runUnitTests();
	cout << "all unit tests passed successfully" << endl;
	/*
	GumdropNode *file = GumdropNode::fromFile("examples/simple.gum");

	cout << file -> toString() << endl;

	cout << file -> getBool("world.dimensions.add_value", false) << endl;
	cout << file -> getBool("world.dimensions.add_value[0]", false) << endl;
	cout << file -> getBool("world.dimensions.add_value[1]", false) << endl;
	cout << file -> getInt("world.dimensions.add_value", 0.0) << endl;

	cout << file -> getBool("world.dimensions[1].add_value", false) << endl;

	cout << "--------------" << endl;
	cout << file -> getDouble("world.dimensions[0].area[0]", 0.0) << endl;
	cout << file -> getDouble("world.dimensions[0].area[1]", 0.0) << endl;
	cout << file -> getString("world.dimensions[0].area[2]", "") << endl;
	cout << file -> getBool("world.dimensions[0].area[3]", true) << endl;

	cout << "--------------" << endl;
	cout << file -> getInt("world.dimensions[0].add_value[0]", 0.0) << endl;
	cout << file -> getInt("world.dimensions[0].add_value[1]", 0.0) << endl;
	cout << file -> getInt("world.dimensions[0].add_value[2]", 0.0) << endl;
	cout << file -> getInt("world.dimensions[0].add_value[3]", 0.0) << endl;
	cout << file -> getInt("world.dimensions[0].add_value[4]", 0.0) << endl;

	cout << "--------------" << endl;
	cout << file -> getInt(GUMDROP("world.dimensions[" << 0 << "].add_value[" << 0 << "]"), 0) << endl;
	cout << file -> getInt(GUMDROP("world.dimensions[" << 0 << "].add_value[" << 1 << "]"), 0) << endl;
	cout << file -> getInt(GUMDROP("world.dimensions[" << 0 << "].add_value[" << 2 << "]"), 0) << endl;
	cout << file -> getInt(GUMDROP("world.dimensions[" << 0 << "].add_value[" << 3 << "]"), 0) << endl;
	cout << file -> getInt(GUMDROP("world.dimensions[" << 0 << "].add_value[" << 4 << "]"), 0) << endl;

	cout << "--------------" << endl;
	cout << file -> getInt("world.dimensions[0].empty[-2]", 1) << endl;

	delete file;*/

	return 0;
}

// - - - function implementations - - - //

void runUnitTests()
{
	GumdropNode *file = GumdropNode::fromFile("examples/unit_tests.gum");

	assert(file);

	assert(file -> getBool("unit_test.some_value", false));									// line 6
	assert(!file -> getBool("unit_test.some_value[1]", false));								// line 6

	assert(file -> getInt("unit_test.some_value", -1) == 0);								// line 3
	assert(file -> getInt("unit_test.some_value[0]", -1) == 0);								// line 3
	assert(file -> getInt("unit_test.some_value[1]", 0) == 1);								// line 4
	assert(file -> getDouble("unit_test.some_value[0]", 0) == 7.8);							// line 7
	assert(file -> getDouble("unit_test.some_value[1]", 0) == 7.9);							// line 5
	assert(file -> getDouble("unit_test.some_value[2]", 0) == 4.0);							// line 5
	assert(file -> getDouble("unit_test.some_value[3]", 0) == 5.0);							// line 5

	assert(file -> getString("unit_test.some_value[2]", "").compare("hello") == 0);			// line 6

	assert(file -> getInt("unit_test.some_node.another", 0) == 2);							// line 12
	assert(file -> getDouble("unit_test.some_node.someVal", 0.0) == 5.0);					// line 13
	assert(file -> getString("unit_test.some_node.msg", "").compare("time") == 0);			// line 14
	assert(file -> getBool("unit_test.some_node.allow", ""));								// line 15

	assert(file -> getInt("unit_test.some_node[1].another", 0) == 3);

	assert(file -> getInt("unit_test[1].some_value", 0) == 8);								// line 26
	assert(file -> getDouble("unit_test[1].some_value", 0.0) == 9.5);						// line 27
	assert(file -> getString("unit_test[1].string_test", "").compare("hello world") == 0);	// line 28

	assert(file -> getInt("test_here", 0) == 5);											// line 31

	delete file;
}
