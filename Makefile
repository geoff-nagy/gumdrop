# Makefile for Gumdrop
# Geoff Nagy

# - - - build flags - - - #
CC=g++
CFLAGS=-c -Wall -Wno-unused-result -g -Og
#CFLAGS=-c -Wall -Wno-unused-result -O3 -s -fexpensive-optimizations
LDFLAGS=
INCLUDES=

# - - - source lists - - - #

# C++ sources go here
CXXSOURCES=main.cpp gumdrop.cpp
CXXOBJECTS=$(CXXSOURCES:.cpp=.o)

# C sources go here
CSOURCES=
COBJECTS=$(CSOURCES:.c=.o)

EXECUTABLE=main
#PREFIX=/usr/local

# - - - build instructions - - - #

all: $(SOURCES) $(EXECUTABLE)

$(EXECUTABLE): $(CXXOBJECTS) $(COBJECTS)
	$(CC) $(CXXOBJECTS) $(COBJECTS) $(LDFLAGS) -o $@

.cpp.o:
	$(CC) $(CFLAGS) $(INCLUDES) $< -o $@

.c.o:
	$(CC) $(CFLAGS) $(INCLUDES) $< -o $@

clean:
	rm $(CXXOBJECTS) $(COBJECTS) $(EXECUTABLE)

# this can be uncommented if you want to hang onto the object files
.INTERMEDIATE: $(CXXOBJECTS) $(COBJECTS)
